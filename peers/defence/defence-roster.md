---
title: Order of Defence
excerpt: Roster of members
---

## Roster by Seniority in the Order


## Arenvald von Hagenburg (Nordmark)
* Elevated as one of three Premiers on 2015/06/27


## Cernac the Inspired (Insulae Draconis)
* Elevated as one of three Premiers on 2015/06/27

## Æiríkr inn Hárfagri (Nordmark)
* Elevated as one of three Premiers on 2015/06/27

## Fardäng Skvaldre (Nordmark)
* Elevated on 2016/01/09

## Alexandre Lerot d'Avignon  (Insulae Draconis)
* Elevated on 2016/05/28
* <a href="http://www.aspiringluddite.com/">An Aspiring Luddite</a>
* Contact: at events or via information on the above-listed site.

## Etienne Fevre de Dion (Insulae Draconis)
* Elevated on 2016/06/04

## Anna von Urwald (Aarnimetsä)
* Elevated on 2017/10/21

## Catlin le Mareschale (Insulae Draconis)
* Elevated on 2018/08/11

## Pól ó Briain (Insulae Draconis)
* Elevated on 2018/12/08